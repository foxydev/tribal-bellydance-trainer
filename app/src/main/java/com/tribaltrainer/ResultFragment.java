package com.tribaltrainer;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tribaltrainer.combo.Combo;
import com.tribaltrainer.combo.FastCombo;
import com.tribaltrainer.combo.SlowCombo;

import java.util.ArrayList;


public class ResultFragment extends Fragment {

    DatabaseHelper dh;
    private MyRecyclerAdapter adapter;

    RecyclerView recyclerView;
    TextView totalCount;

    int num;

    ArrayList<Combo> combos;

    static ResultFragment newInstance(int num) {

        ResultFragment f = new ResultFragment();
        Bundle args = new Bundle();
        args.putInt("num", num);
        f.setArguments(args);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.swipe_result, container, false);

        totalCount = (TextView) v.findViewById(R.id.total_count);
        recyclerView = (RecyclerView) v.findViewById(R.id.result_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);

        Combo c = combos.get(num);
        String countText;
        if(c.getComboType().equals("Fast")) {
            countText = String.format(getResources().getString(R.string.fast_total_count), c.getTotalCount());
            totalCount.setText(countText);
        } else {
            countText = getResources().getString(R.string.slow_total_count);
            totalCount.setText(countText);
        }
        return v;
    }

    @Override
    public void onCreate(Bundle savedInstanceState){

        super.onCreate(savedInstanceState);
        dh = new DatabaseHelper(getContext());
        num = getArguments() != null ? getArguments().getInt("num") : 1;

        if(num > combos.size() - 1)
            combos.add(num, getCombo());

        Combo c = combos.get(num);
        adapter = new MyRecyclerAdapter(c.getStepList(), c.getComboType());
    }

    public Combo getCombo() {

        Bundle args = super.getActivity().getIntent().getExtras();
        String tempo = args.getString("_Tempo");
        Combo combo;

        if (tempo != null && tempo.equals("_Tempo = 7"))
            combo = new FastCombo(dh, args);
        else
            combo = new SlowCombo(dh, args);
        return combo;
    }
}
