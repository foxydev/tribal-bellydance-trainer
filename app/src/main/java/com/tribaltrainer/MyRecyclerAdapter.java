package com.tribaltrainer;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class MyRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private LayoutInflater inflater;
    List<MoveItem> data;
    String comboType;

    public MyRecyclerAdapter(List<MoveItem> data, String comboType) {
        this.data = data;
        this.comboType = comboType;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        inflater = LayoutInflater.from(parent.getContext());
        View view;
        RecyclerView.ViewHolder myViewHolder;

        if(comboType.equals("Fast")) {
            view = inflater.inflate(R.layout.custom_row_fast, parent, false);
            myViewHolder = new MyViewHolderFast(view);
        }else{
            view = inflater.inflate(R.layout.custom_row_slow, parent, false);
            myViewHolder = new MyViewHolderSlow(view);
        }
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        MoveItem current = data.get(position);

        if(comboType.equals("Fast")) {
            MyViewHolderFast holderFast = (MyViewHolderFast) holder;
            holderFast.textViewName.setText(current.getName());
            holderFast.textViewCount.setText(String.valueOf(current.getmCount()));
        }else{
            MyViewHolderSlow holderSlow = (MyViewHolderSlow) holder;
            holderSlow.textViewName.setText(current.getName());
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyViewHolderFast extends RecyclerView.ViewHolder {

        TextView textViewName;
        TextView textViewCount;

        public MyViewHolderFast(View itemView) {
            super(itemView);
            textViewName = (TextView) itemView.findViewById(R.id.text_view_name);
            textViewCount = (TextView) itemView.findViewById(R.id.text_view_count);
        }
    }

    class MyViewHolderSlow extends RecyclerView.ViewHolder {

        TextView textViewName;

        public MyViewHolderSlow(View itemView) {
            super(itemView);
            textViewName = (TextView) itemView.findViewById(R.id.text_view_name);
        }
    }
}
