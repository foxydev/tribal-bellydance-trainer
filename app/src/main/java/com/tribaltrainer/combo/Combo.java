package com.tribaltrainer.combo;

import android.database.Cursor;
import android.os.Bundle;

import com.tribaltrainer.DatabaseHelper;
import com.tribaltrainer.MoveItem;

import java.util.ArrayList;

/**
 * Created by admin on 9/8/2015.
 */
public abstract class Combo {

    protected Bundle args;
    protected ArrayList<String> steps;
    protected ArrayList<MoveItem> stepList;
    protected DatabaseHelper databaseHelper;
    protected int totalCount;
    protected int maxCount;
    protected String comboType;

    protected String queryCombo, queryTribal;

    abstract void addStep(Cursor cursor);
    abstract void addCombo(Cursor cursor);
    abstract ArrayList<MoveItem> createSteps();

    public Combo(DatabaseHelper databaseHelper, Bundle args) {
        steps = new ArrayList<>();
        stepList = new ArrayList<>();
        totalCount = 0;
        maxCount = 32;

        this.args = args;
        setdatabaseHelper(databaseHelper);
        buildQuery();
        createSteps();
    }

    public void setdatabaseHelper(DatabaseHelper databaseHelper) {
        this.databaseHelper = databaseHelper;
    }

    public void checkCount(int count) {
        switch (count) {
            case 8:
                maxCount = 16;
                break;
            case 24:
                maxCount = 32;
                break;
            case 40:
                maxCount = 48;
                break;
            case 56:
                maxCount = 64;
                break;
        }
        totalCount += count;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public ArrayList<MoveItem> getStepList() {
        return stepList;
    }

    public String getComboType() {
        return comboType;
    }

    protected void buildQuery()
    {
        String level = args.getString("_Level");
        String tempo = args.getString("_Tempo");
        String format = "SELECT * FROM %s WHERE (%s) AND %s ORDER BY RANDOM() LIMIT %d";

        queryCombo  = String.format(format, "combo", level, tempo, maxCount);
        queryTribal = String.format(format, "tribalapp", level,
                tempo + " AND _Single = 1 AND _Formation = 8", maxCount);

    }

}
