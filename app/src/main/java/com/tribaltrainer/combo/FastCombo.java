package com.tribaltrainer.combo;

import android.database.Cursor;
import android.os.Bundle;
import android.os.Debug;
import android.util.Log;

import com.tribaltrainer.DatabaseHelper;
import com.tribaltrainer.MoveItem;

import java.util.ArrayList;

/**
 * Created by admin on 9/8/2015.
 */
public class FastCombo extends Combo {

    public FastCombo(DatabaseHelper databaseHelper, Bundle args) {
        super(databaseHelper, args);
        comboType = "Fast";
    }
    
    @Override
    void addStep(Cursor cursor) {
        MoveItem moveItem = new MoveItem();
        String step = cursor.getString(cursor.getColumnIndex("_Name"));
        moveItem.setName(step);
        int count = cursor.getInt(cursor.getColumnIndex("_Count"));
        moveItem.setmCount(count);
        checkCount(count);
        steps.add(step);
        stepList.add(moveItem);
    }

    @Override
    void addCombo(Cursor cursor) {
        int count = cursor.getInt(cursor.getColumnIndex("_Count"));
        checkCount(count);
        String combo = cursor.getString(cursor.getColumnIndex("_Combo"));
        String sepCounts = cursor.getString(cursor.getColumnIndex("_SeparateCounts"));
        String[] moves = combo.split(";");
        String[] separateCounts = sepCounts.split(";");

        for (int i = 0; i < moves.length && i < separateCounts.length; i++) {
            MoveItem moveItem = new MoveItem();
            steps.add(moves[i]);
            moveItem.setName(moves[i]);
            moveItem.setmCount(Integer.valueOf(separateCounts[i]));
            stepList.add(moveItem);
        }
    }

    @Override
    ArrayList<MoveItem> createSteps() {
        stepList.clear();
        steps.clear();

        Cursor cursor = databaseHelper.getCursor(queryCombo);
        if (cursor.moveToFirst())
            addCombo(cursor);

        cursor = databaseHelper.getCursor(queryTribal);

        int count, limit;
        int tCountIdx = cursor.getColumnIndex("_Count");
        while(cursor.moveToNext()){
            count = cursor.getInt(tCountIdx);
            limit = totalCount + count;
            if(limit % 8 == 0 && limit <= maxCount)
                addStep(cursor);
        }
        Log.e("TOTALCOUNT", "" + totalCount);

        cursor.close();
        return stepList;
    }
}
