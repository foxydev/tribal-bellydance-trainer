package com.tribaltrainer.combo;

import android.database.Cursor;
import android.os.Bundle;

import com.tribaltrainer.DatabaseHelper;
import com.tribaltrainer.MoveItem;

import java.util.ArrayList;

/**
 * Created by admin on 9/8/2015.
 */
public class SlowCombo extends Combo {

    public SlowCombo(DatabaseHelper databaseHelper, Bundle args) {
        super(databaseHelper, args);
        comboType = "Slow";
    }

    @Override
    void addStep(Cursor cursor) {
        MoveItem moveItem = new MoveItem();
        String step = cursor.getString(cursor.getColumnIndex("_Name"));
        moveItem.setName(step);
        steps.add(step);
        stepList.add(moveItem);
    }

    @Override
    void addCombo(Cursor cursor) {
        String combo = cursor.getString(cursor.getColumnIndex("_Combo"));
        String[] moves = combo.split(";");
        for (int i = 0; i < moves.length; i++) {
            MoveItem moveItem = new MoveItem();
            steps.add(moves[i]);
            moveItem.setName(moves[i]);
            stepList.add(moveItem);
        }
    }

    @Override
    ArrayList<MoveItem> createSteps() {
        stepList.clear();
        steps.clear();

        Cursor cursor = databaseHelper.getCursor(queryCombo);
        if (cursor.moveToFirst())
            addCombo(cursor);

        cursor = databaseHelper.getCursor(queryTribal);
        cursor.moveToFirst();
        int limit = 8 - stepList.size();
        for(int i = 0; i < limit; i++) {
            if(cursor.moveToNext())
                addStep(cursor);
        }

        cursor.close();
        return stepList;
    }
}
