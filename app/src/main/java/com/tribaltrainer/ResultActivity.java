package com.tribaltrainer;

import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;

import android.widget.Toast;

import com.tribaltrainer.combo.Combo;

import java.util.ArrayList;


/**
 * Created by admin on 10/2/2015.
 */
public class ResultActivity extends FragmentActivity {

    MyAdapter pagerAdapter;
    ViewPager viewPager;

    ArrayList<Combo> combos;
    String tempo;

    final String PREFS_NAME = "MyPrefs";

    private static final int CONTENT_VIEW_ID = 10101010;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.swipe_layout_main);

        combos = new ArrayList<>();
        Bundle args = getIntent().getExtras();
        tempo = args.getString("_Tempo");
        pagerAdapter = new MyAdapter(getSupportFragmentManager());
        viewPager = (ViewPager) findViewById(R.id.pager);
        viewPager.setAdapter(pagerAdapter);
/*
        SharedPreferences prefs = getSharedPreferences(PREFS_NAME, 0);
        if(prefs.getBoolean("first_launch", true)) {
            prefs.edit().putBoolean("first_launch", false).apply();

            FragmentManager fm = getSupportFragmentManager();
            tutorialFragment = fm.findFragmentById(R.layout.splash)

            Toast.makeText(this, "Swipe BACK and FORWARD", Toast.LENGTH_LONG).show();
        }*/
    }

    public class MyAdapter extends FragmentStatePagerAdapter {

        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            ResultFragment f = ResultFragment.newInstance(position);
            f.combos = combos;
            return  f;
        }

        @Override
        public int getCount() {
            return 100;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Bundle args = getIntent().getExtras();
        String tempo = args.getString("_Tempo");
        if(!this.tempo.equals(tempo)) {
            combos.clear();
        }
    }
}
