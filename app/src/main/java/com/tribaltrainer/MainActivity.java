package com.tribaltrainer;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.RadioButton;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    static CheckBox[] chbLevel;
    static RadioButton rbFast, rbSlow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        chbLevel = new CheckBox[3];
        chbLevel[0] = (CheckBox) findViewById(R.id.cb1);
        chbLevel[1] = (CheckBox) findViewById(R.id.cb2);
        chbLevel[2] = (CheckBox) findViewById(R.id.cb3);
        rbFast = (RadioButton) findViewById(R.id.rb1);
        rbSlow = (RadioButton) findViewById(R.id.rb2);
    }

    @Override
    public void onClick(View v) {

        String level = "";

        int checked = 0;
        for (int i = 0; i < 3; i++) {
            if (chbLevel[i].isChecked()) {
                if( checked > 0 && i <= 2)
                    level += " OR ";
                level = level + String.format("_Level = %d", i + 1);
                checked++;
            }
        }

        if( checked == 0 || checked == 3 )
            level = "_Level > 0";

        int n = rbFast.isChecked() ? 7 : 5;
        String tempo = String.format("_Tempo = %d", n);

        Intent intent = new Intent(this, ResultActivity.class);
        intent.putExtra("_Level", level);
        intent.putExtra("_Tempo", tempo);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_about) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(R.string.dialog_message)
                    .setTitle(R.string.dialog_title);
            builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
            return true;
        }

        if (id == android.R.id.home) {
            NavUtils.shouldUpRecreateTask(this, getParentActivityIntent());
        }

        return super.onOptionsItemSelected(item);
    }


}
