package com.tribaltrainer;

import android.os.Parcel;
import android.os.Parcelable;

public class MoveItem implements Parcelable {
    private int mCount;
    private String name;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mCount);
        dest.writeString(name);
    }

    public static final Parcelable.Creator<MoveItem> CREATOR
            = new Parcelable.Creator<MoveItem>() {
        public MoveItem createFromParcel(Parcel in) {
            return new MoveItem(in);
        }

        public MoveItem[] newArray(int size) {
            return new MoveItem[size];
        }
    };

    private MoveItem(Parcel in) {
        mCount = in.readInt();
        name = in.readString();
    }

    public MoveItem() {

    }

    public int getmCount() {
        return mCount;
    }

    public void setmCount(int mCount) {
        this.mCount = mCount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
